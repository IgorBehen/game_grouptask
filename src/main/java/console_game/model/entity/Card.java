package console_game.model.entity;

public enum Card {
  Mr_Bigglesworth("Mr Bigglesworth",1, 1, 1),
  Murloc_Tinyfin("Murloc Tinyfin", 1 ,1 ,1 ),
  Sheep("Sheep",1,1,1),
  Timber_Wolf("Timber Wolf", 1, 1, 1),
  Angry_Chicken("Angry_Chicken", 1 ,1 ,1 ),
  Voidwalker("Voidwalker", 1 ,3 ,1 ),
  Slime("Slime", 2, 2, 2),
  Cruel_Taskmaster("Cruel Taskmaster", 2 ,2 ,2 ),
  Keeper_Stalladris("Keeper Stalladris", 2 ,3 ,2 ),
  Stargazer_Luna("Stargazer Luna", 2 ,4 ,3 ),
  Edwin_VanCleef("Edwin VanCleef", 2 ,2 ,3 ),
  SN1P_SN4P("SN1P-SN4P", 2 ,3 ,3 ),
  Black_Cat("Black Cat", 3 ,3 ,3 ),
  Ctypt_Lord("Ctypt Lord", 1 ,6 ,3 ),
  Murloc_Knight("Murloc Knight", 3 ,4 ,4),
  Humongous_Razorleaf("Humongous Razorleaf", 4 ,8 ,4 ),
  Harrison_Jones("Harrison Jones", 5 ,4 ,5 ),
  Leeroy_Jenkins("Leeroy Jenkins", 6 ,2 ,5 ),
  Swampqueen_Hagatha("Swampqueen Hagatha", 5 ,5 ,7 ),
  Archmage_Antonidas("Archmage Antonidas", 5 ,7 ,7 );

  String name;
  int power;
  int health;
  int manaCost;

  Card(String name, int power, int health, int manaCost) {
    this.name = name;
  this.power = power;
  this.health = health;
  this.manaCost = manaCost;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getManaCost() {
    return manaCost;
  }

  public void setManaCost(int manaCost) {
    this.manaCost = manaCost;
  }

  public int getPower() {
    return this.power;
  }

  public void setPower(int power) {
    this.power = power;
  }

  public int getHealth() {
    return health;
  }

  public void setHealth(int health) {
    this.health = health;
  }
}
